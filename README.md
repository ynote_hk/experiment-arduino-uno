# Experiment Arduino Uno

> Experiment from february 2021
>
> _You can check out my article to know more about [how I document my process when
> I'm testing some new tech stuff](https://code.likeagirl.io/the-art-of-documenting-your-coding-experience-e868e28cdc45)._

This is a small project that intends to test Johnny-Five by writing some little
scripts for an Arduino Uno.

- [Results](#results)
- [Steps](#steps)
- [Requirements](#requirements)
- [Usage](#usage)
- [Resources](#resources)

## Results 📝

- On [WSL](https://github.com/Microsoft/WSL), there is no USB pass-through
  access (cf. https://github.com/microsoft/WSL/issues/5158). As I switched on my
  brother's Windows Surface, I am using WSL. So, at first, my
  `hello-world` doesn't work at all  :tired_face:. Too bad! I'm going to install
  a Debian on a proper virtual machine.

## Steps

### Install the dependencies and try a simple script
- Install Johnny-Five.
- Switch the repository to Windows and run Node with Powershell.
- Learn basic API methods on [`led`](http://johnny-five.io/api/led/).

## Requirements
- Node v15.6.0

## Usage

Install the dependencies:
```sh
npm i
```

**Hello World**

```sh
# Simple hello world that blink led 13
node hello-world/index.js

# Manipulate a led through led 13
node hello-world/led.js

# >> led.on()
# >> led.off()
# >> led.blink()
# >> led.stop()

```

## Resources
