const { Board, Led } = require('johnny-five')

const board = new Board()

board.on('ready', function() {
  console.log('Ready event. Repl instance auto-initialized!')

  const led = new Led(13)

  this.repl.inject({
    on: function() {
      led.on()
    },
    off: function() {
      led.off()
    }
  })
})
